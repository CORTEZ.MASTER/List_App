package com.example.cortez.list_app;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class Main2Activity extends AppCompatActivity {

    ListView ls;

    String[] nome = {"Kouga de Pégaso", "Souma de Lionet","Yuna de Águia", "Ryuho de Dragão",
    "Haruto de Lobo", "Eden de Órion", "Celeris de Cavalo", "Kitalpha de Cavalo", "Spear de Espadarte","Argo de Peixe Voador"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //Deixar celular somente na vertical
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ls = (ListView) findViewById(R.id.List_nomes);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, nome);
        ls.setAdapter(adapter);

        ls.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Toast.makeText(getApplicationContext(), "O Cavaleiro escolhido foi o " +(i+1), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
